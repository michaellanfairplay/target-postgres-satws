"""PostgresSatws target class."""

from singer_sdk.target_base import Target
from singer_sdk import typing as th

from target_postgres_satws.sinks import (
    PostgresSatwsSink,
)


class TargetPostgresSatws(Target):
    """Sample target for PostgresSatws."""

    name = "target-postgres-satws"
    # config_jsonschema = th.PropertiesList(
    #     th.Property(
    #         "host",
    #         th.StringType,
    #         required=True,
    #     ),
    #     th.Property(
    #         "port",
    #         th.NumberType,
    #         required=True,
    #     ),
    #     th.Property(
    #         "user",
    #         th.StringType,
    #         required=True,
    #     ),
    #     th.Property(
    #         "password",
    #         th.StringType,
    #         required=True,
    #     ),
    #     th.Property(
    #         "dbname",
    #         th.StringType,
    #         required=True,
    #     ),
    # ).to_dict()
    default_sink_class = PostgresSatwsSink
