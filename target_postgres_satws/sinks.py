"""PostgresSatws target sink class, which handles writing streams."""
import uuid
from datetime import datetime

from peewee import DoesNotExist
from singer_sdk.sinks import RecordSink

from target_postgres_satws.database import Invoices, InvoicesItems, InvoicesRelations, SuppliersCompany


class PostgresSatwsSink(RecordSink):
    """PostgresSatws target sink class."""

    def process_record(self, record: dict, context: dict) -> None:
        """Process the record."""
        record['canceledAt'] = datetime.strptime(record['canceledAt'], "%Y-%m-%d %H:%M:%S") if record['canceledAt'] else None
        record['certifiedAt'] = datetime.strptime(record['certifiedAt'], "%Y-%m-%d %H:%M:%S")
        record['createdAt'] = datetime.strptime(record['createdAt'], "%Y-%m-%d %H:%M:%S")
        record['fullyPaidAt'] = datetime.strptime(record['fullyPaidAt'], "%Y-%m-%d %H:%M:%S") if record['fullyPaidAt'] else None
        record['issuedAt'] = datetime.strptime(record['issuedAt'], "%Y-%m-%d %H:%M:%S")
        record['lastPaymentDate'] = datetime.strptime(record['lastPaymentDate'], "%Y-%m-%d %H:%M:%S") if record['lastPaymentDate'] else None
        record['updatedAt'] = datetime.strptime(record['updatedAt'], "%Y-%m-%d %H:%M:%S")

        tax_id = record['receiver'] if record['isIssuer'] else record['issuer']
        try:
            supplier_id = SuppliersCompany.get(SuppliersCompany.tax_id == tax_id)
            supplier_id = supplier_id.id
        except DoesNotExist:
            supplier_id = None

        try:
            row = Invoices.get_by_id(record['id'])
            if record['status'] != row.status:
                Invoices.set_by_id(record['id'], {'status': record['status']})
        except DoesNotExist:
            Invoices.create(
                id=record['id'],
                sat_ws_id=record['id'],
                canceled_at=record['canceledAt'],
                cancellation_process_status=record['cancellationProcessStatus'],
                cancellation_status=record['cancellationStatus'],
                certified_at=record['certifiedAt'],
                company_id=record['company_id'],
                connector_id=record['connector_id'],
                created=record['createdAt'],
                currency=record['currency'],
                discount=record['discount'],
                due_amount=record['dueAmount'],
                exchange_rate=record['exchangeRate'],
                fully_paid_at=record['fullyPaidAt'],
                internal_identifier=record['internalIdentifier'],
                issuer=record['issuer'],
                receiver=record['receiver'],
                is_issuer=record['isIssuer'],
                is_receiver=record['isReceiver'],
                is_removed=False,
                issued_at=record['issuedAt'],
                last_payment_day=record['lastPaymentDate'],
                modified=record['updatedAt'],
                pac=record['pac'],
                paid_amount=record['paidAmount'],
                payment_method=record['paymentMethod'],
                payment_type=record['paymentType'],
                pdf=record['pdf'],
                place_of_issue=record['placeOfIssue'],
                reference=record['reference'],
                status=record['status'],
                subtotal=record['subtotal'],
                supplier_id=supplier_id,
                tax=record['tax'],
                total=record['total'],
                type=record['type'],
                usage=record['usage'],
                uuid=record['uuid'],
                version=record['version'],
                xml=record['xml'],
            )

            for item in record['items']:
                InvoicesItems.create(
                    id=str(uuid.uuid4()),
                    created=record['createdAt'],
                    description=item['description'],
                    discount_amount=item['discountAmount'],
                    identification_number=item['identificationNumber'],
                    invoice_id=record['id'],
                    is_removed=False,
                    modified=record['updatedAt'],
                    product_identification=item['productIdentification'],
                    quantity=item['quantity'],
                    tax_amount=item['taxAmount'],
                    tax_rate=item['taxRate'],
                    tax_type=item['taxType'],
                    total_amount=item['totalAmount'],
                    unit_amount=item['unitAmount'],
                    unit_code=item['unitCode'],
                )

            for relation in record['relations']:
                relation['createdAt'] = datetime.strptime(relation['createdAt'], "%Y-%m-%d %H:%M:%S") if relation['createdAt'] else None
                relation['updatedAt'] = datetime.strptime(relation['updatedAt'], "%Y-%m-%d %H:%M:%S") if relation['updatedAt'] else None

                InvoicesRelations.create(
                    id=relation['@id'].split('/')[-1],
                    created=relation['createdAt'],
                    invoice_id=record['id'],
                    is_removed=False,
                    modified=relation['updatedAt'],
                    related_invoice=relation['relatedInvoiceUuid'],
                    type=relation['type'],
                    )
