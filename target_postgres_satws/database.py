"""Models an database for target"""

from urllib.parse import urlparse, ParseResult
from os import environ

from playhouse.postgres_ext import PostgresqlExtDatabase, BinaryJSONField
from peewee import Model, UUIDField, DateTimeField, CharField, DoubleField, BooleanField, \
    IntegerField, SmallIntegerField, TextField, BigIntegerField

INSIGHTS_DB_URI: str = environ["DATABASE_URL"]
INSIGHTS_DB_URI: ParseResult = urlparse(INSIGHTS_DB_URI)

insights: PostgresqlExtDatabase = PostgresqlExtDatabase(
    user=INSIGHTS_DB_URI.username,
    password=INSIGHTS_DB_URI.password,
    host=INSIGHTS_DB_URI.hostname,
    port=INSIGHTS_DB_URI.port or 5432,
    database=str(INSIGHTS_DB_URI.path)[1:],
)


class BaseInvoices(Model):
    class Meta:
        database = insights


class Invoices(BaseInvoices):
    id = UUIDField(primary_key=True)
    canceled_at = DateTimeField(null=True)
    cancellation_process_status = CharField(null=True)
    cancellation_status = CharField(null=True)
    certified_at = DateTimeField(null=True)
    company_id = UUIDField(index=True, null=True)
    connector_id = UUIDField(index=True, null=True)
    created = DateTimeField()
    currency = CharField(null=True)
    discount = DoubleField(null=True)
    due_amount = DoubleField(null=True)
    exchange_rate = DoubleField(null=True)
    fully_paid_at = DateTimeField(null=True)
    internal_identifier = CharField(null=True)
    is_issuer = BooleanField(null=True)
    is_paid = BooleanField(null=True)
    is_receiver = BooleanField(null=True)
    is_removed = BooleanField()
    issued_at = DateTimeField(null=True)
    issuer = BinaryJSONField(null=True)
    last_payment_day = DateTimeField(null=True)
    modified = DateTimeField()
    pac = CharField(null=True)
    paid_amount = DoubleField(null=True)
    payment_method = CharField(null=True)
    payment_type = CharField(null=True)
    pdf = BooleanField(null=True)
    place_of_issue = CharField(null=True)
    receiver = BinaryJSONField(null=True)
    reference = CharField(null=True)
    sat_ws_id = UUIDField(null=True)
    status = CharField(null=True)
    subtotal = DoubleField(null=True)
    supplier_id = UUIDField(index=True, null=True)
    tax = DoubleField(null=True)
    total = DoubleField(null=True)
    type = CharField(null=True)
    usage = CharField(null=True)
    uuid = UUIDField(null=True)
    version = DoubleField(null=True)
    xml = BooleanField(null=True)

    class Meta:
        table_name = 'invoices'


class InvoicesItems(BaseInvoices):
    id = UUIDField(primary_key=True)
    created = DateTimeField()
    description = CharField(null=True)
    discount_amount = DoubleField(null=True)
    identification_number = CharField(null=True)
    invoice_id = UUIDField(index=True, null=True)
    is_removed = BooleanField()
    modified = DateTimeField()
    product_identification = CharField(null=True)
    quantity = DoubleField(null=True)
    tax_amount = DoubleField(null=True)
    tax_rate = DoubleField(null=True)
    tax_type = CharField(null=True)
    total_amount = DoubleField(null=True)
    unit_amount = IntegerField(null=True)
    unit_code = CharField(null=True)

    class Meta:
        table_name = 'invoices_items'


class InvoicesRelations(BaseInvoices):
    id = UUIDField(primary_key=True)
    created = DateTimeField()
    invoice_id = UUIDField(index=True)
    is_removed = BooleanField()
    modified = DateTimeField()
    related_invoice = UUIDField(index=True)
    type = SmallIntegerField()

    class Meta:
        table_name = 'invoices_relations'

class SuppliersCompany(BaseInvoices):
    address = CharField(null=True)
    alias = CharField()
    comments = TextField(null=True)
    company_id = UUIDField(index=True)
    country_id = BigIntegerField(index=True, null=True)
    created = DateTimeField()
    created_by_id = UUIDField(index=True, null=True)
    id = UUIDField(primary_key=True)
    is_removed = BooleanField()
    main_activity = CharField()
    modified = DateTimeField()
    registered_name = CharField(null=True)
    state = CharField(null=True)
    status = CharField()
    supplier_id = UUIDField(index=True, null=True)
    tax_id = CharField(null=True)
    type = CharField(null=True)
    website = CharField(null=True)

    class Meta:
        table_name = 'suppliers_company'
